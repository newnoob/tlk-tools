import React, { Component } from 'react';
import './App.css';

const LEVEL_STD = "STD";
const LEVEL_VET = "VET";
const LEVEL_MST = "MST";

const EUROPE_MAP = 0;
const ASIA_MAP = 1;
const ROME_MAP = 2;

const MARQUESS_DUX = 0;
const COUNT_LEGATE = 1;
const BARON_PREFECT = 2;
const MARSHAL_TRIBUNUSCOHORTIS = 3;
const VICEMARSHAL_TRIBUNUS = 4;

function UnitListEntry(props) {
  return (
    <tbody id={props.name} class="UnitListEntry">
      <UnitListEntryMain row={props.row} tier={props.tier} name={props.name} hp={props.hp} str={props.str} rstr={props.rstr} auth={props.auth} />
      <UnitListEntryExtension row={props.row + 1} tier={props.tier} level={props.tier === 1 ? LEVEL_STD : LEVEL_VET} hp={props.hp} str={props.str} rstr={props.rstr} auth={props.auth} />
      <UnitListEntryExtension row={props.row + 2} tier={props.tier} level={props.tier === 1 ? LEVEL_VET : LEVEL_MST} hp={props.hp} str={props.str} rstr={props.rstr} auth={props.auth} />
    </tbody>);
}

function UnitListEntryMain(props) {
  return (<tr className="UnitListEntryMain" style={{backgroundColor: alternateBackground(props.row, props.tier)}}>
      <td>{props.name}</td>
      <td>{props.hp}</td>
      <td>{props.str}</td>
      <td>{props.rstr}</td>
      <td>{props.auth}</td>
    </tr>);
}

function UnitListEntryExtension(props) {
  return (<tr className="UnitListEntryExtension">
      <td>{props.level}</td>
      <td>{calculateHP(props.hp, props.tier, props.level)}</td>
      <td>{calculateSTR(props.str, props.tier, props.level)}</td>
      <td>{calculateSTR(props.rstr, props.tier, props.level)}</td>
      <td>{calculateAuth(props.auth, props.tier, props.level)}</td>
    </tr>);
}

function alternateBackground(row, tier) {
  switch (tier) {
    case 1:
    return row % 2 === 0 ? '#fff' : '#ddd';
    case 2:
    return row % 2 === 0 ? '#ccc' : '#aaa';
    default:
    return row % 2 === 0 ? '#999' : '#777';
  }
}

function calculateHP(hp, tier, level) {
  var calcHp = 0;
  if (tier === 1) {
    if (level === LEVEL_STD) {
      calcHp = hp * 1.11;
    } else if (level === LEVEL_VET) {
      calcHp = hp * 1.28;
    }
  } else {
    if (level === LEVEL_VET) {
      calcHp = hp * 1.12;
    } else if (level === LEVEL_MST) {
      calcHp = hp * 1.24;
    }
  }
  calcHp = Math.round(calcHp);
  return calcHp;
}

function calculateSTR(str, tier, level) {
  var calcStr = 0;
  if (tier === 1) {
    if (level === LEVEL_STD) {
      calcStr = str * 1.25;
    } else if (level === LEVEL_VET) {
      calcStr = str * 1.56;
    }
  } else {
    if (level === LEVEL_VET) {
      calcStr = str * 1.15;
    } else if (level === LEVEL_MST) {
      calcStr = str * 1.3;
    }
  }
  calcStr = Math.round(calcStr);
  return calcStr;
}

function calculateAuth(auth, tier, level) {
  var calcAuth = 0;
  switch (tier) {
    case 1:
      if (level === LEVEL_STD) {
        calcAuth = auth * 1.11;
      } else if (level === LEVEL_VET) {
        calcAuth = auth * 1.33;
      }
      break;
    case 2:
      if (level === LEVEL_VET) {
        calcAuth = auth * 1.13;
      } else if (level === LEVEL_MST) {
        calcAuth = auth * 1.26;
      }
      break;
    case 3:
      if (level === LEVEL_VET) {
        calcAuth = auth * 1.15;
      } else if (level === LEVEL_MST) {
        calcAuth = auth * 1.3;
      }
      break;
    default:
      calcAuth = auth;
      break;
  }
  calcAuth = Math.round(calcAuth);
  return calcAuth;
}

function UnitListInstruction() {
  return (<p>Hover over unit for stats after promotion.</p>);
}

function EuropeUnitList() {
  var row = 0;
  return (<table cellspacing="0" className="UnitList">
    <thead>
      <tr style={{backgroundColor: 'black', 'color': 'white'}}>
        <th id="name">NAME</th>
        <th id="hp">HP</th>
        <th id="str">STR</th>
        <th id="rstr">RSTR</th>
        <th id="auth">AUTH</th>
      </tr>
    </thead>
      <UnitListEntry row={row++} tier={1} name="Ballista" hp={90} str={0} rstr={52} auth={63} />
      <UnitListEntry row={row++} tier={1} name="Bedouin" hp={90} str={10} rstr={0} auth={29} />
      <UnitListEntry row={row++} tier={1} name="Crossbowman" hp={100} str={12} rstr={16} auth={54} />
      <UnitListEntry row={row++} tier={1} name="Footman" hp={95} str={10} rstr={0} auth={32} />
      <UnitListEntry row={row++} tier={1} name="Horseman" hp={90} str={9} rstr={0} auth={45} />
      <UnitListEntry row={row++} tier={1} name="Peasant" hp={72} str={6} rstr={0} auth={14} />
      <UnitListEntry row={row++} tier={1} name="Scout" hp={80} str={8} rstr={7} auth={41} />
      <UnitListEntry row={row++} tier={1} name="Slinger" hp={90} str={8} rstr={12} auth={36} />
      <UnitListEntry row={row++} tier={1} name="Spearman" hp={85} str={11} rstr={0} auth={45} />
      <UnitListEntry row={row++} tier={1} name="Templar" hp={125} str={12} rstr={0} auth={66} />
      <UnitListEntry row={row++} tier={1} name="Viking" hp={95} str={16} rstr={0} auth={54} />
      <UnitListEntry row={row++} tier={2} name="Arblaster" hp={135} str={23} rstr={33} auth={80} />
      <UnitListEntry row={row++} tier={2} name="Archer" hp={120} str={12} rstr={34} auth={54} />
      <UnitListEntry row={row++} tier={2} name="Azab" hp={140} str={18} rstr={0} auth={54} />
      <UnitListEntry row={row++} tier={2} name="Camel" hp={125} str={14} rstr={10} auth={60} />
      <UnitListEntry row={row++} tier={2} name="Catapult" hp={125} str={0} rstr={90} auth={90} />
      <UnitListEntry row={row++} tier={2} name="Double Crossbowman" hp={135} str={15} rstr={60} auth={80} />
      <UnitListEntry row={row++} tier={2} name="Horse Archer" hp={135} str={11} rstr={33} auth={70} />
      <UnitListEntry row={row++} tier={2} name="Knight" hp={160} str={23} rstr={0} auth={100} />
      <UnitListEntry row={row++} tier={2} name="Lancer" hp={140} str={21} rstr={0} auth={100} />
      <UnitListEntry row={row++} tier={2} name="Militia" hp={100} str={10} rstr={0} auth={23} />
      <UnitListEntry row={row++} tier={2} name="Pikeman" hp={125} str={19} rstr={0} auth={70} />
      <UnitListEntry row={row++} tier={2} name="Skirmisher" hp={92} str={11} rstr={8} auth={25} />
    </table>);
}

function MapMenuBar(props) {
  return (<ul id="map" className="HorizontalMenuBar">
    <li className="MapMenuItem"><button name="EUROPE" onClick={props.onClick}>EUROPE</button></li>
    <li className="MapMenuItem"><button name="ASIA" onClick={props.onClick}>ASIA</button></li>
    <li className="MapMenuItem"><button name="ROME" onClick={props.onClick}>ROME</button></li>
    </ul>);
}

function UnitList(props) {
    switch(props.map) {
      case EUROPE_MAP:
        return (<EuropeUnitList />);
      case ASIA_MAP:
        return (<div></div>);
      case ROME_MAP:
        return (<div></div>);
      default:
        return;
    }
}

function ToolMenuBar(props) {
  return (<ul id="tool" className="HorizontalMenuBar"></ul>);
}

class MixCalculator extends Component {
  constructor(props) {
    super(props);
    this.addUnit = this.addUnit.bind(this);
    this.clearUnit = this.clearUnit.bind(this);
    this.state = {rank : MARQUESS_DUX};
  }

  addUnit(unit) {}
  clearUnit() {}

  render() {
    return (<MixRankSelector map={this.props.map}/>);
  }
}

function MixRankSelector(props) {
  if (props.map === ROME_MAP) {
    return (<RomeRankDropdown />);
  } else {
    return (<EuropeAsiaRankDropdown />);
  }
}

function EuropeAsiaRankDropdown(props) {
  return (<select>
    <option value="marquess">Marquess</option>
    <option value="count">Count</option>
    <option value="baron">Baron</option>
    <option value="marshal">Marshal</option>
    <option value="vicemarshal">Vice Marshal</option>
  </select>);
}

function RomeRankDropdown(props) {
  return (<select>
    <option value="marquess">Dux</option>
    <option value="count">Legate</option>
    <option value="baron">Prefect</option>
    <option value="marshal">Tribunus Cohortis</option>
    <option value="vicemarshal">Tribunus</option>
  </select>);
}

class App extends Component {
  constructor(props) {
    super(props);
    this.changeMap = this.changeMap.bind(this);
    this.state = {map : EUROPE_MAP};
  }

  changeMap(e) {
    if (e.target.name === "EUROPE") {
      this.setState({map: EUROPE_MAP});
    } else if (e.target.name === "ASIA") {
      this.setState({map: ASIA_MAP});
    } else if (e.target.name === "ROME") {
      this.setState({map: ROME_MAP});
    }
  }

  render() {
    return (
      <div className="App">
        <MapMenuBar onClick={this.changeMap} />
        <UnitListInstruction />
        <UnitList map={this.state.map} />
        <MixCalculator map={this.state.map}/>
      </div>
    );
  }
}



export default App;
